//
//  GalleryApi.swift
//  FlickrGallery
//
//  Created by The Admin on 2021-09-07.
//

import Foundation

public protocol GalleryApi: ObservableObject {
    
    func getPhotosPage(page: Int?)
    
}
