//
//  FlickrApi.swift
//  FlickrGallery
//
//  Created by The Admin on 2021-09-07.
//

import Foundation
import Alamofire

public struct PhotosRequestResult: Decodable {
    public var photos: PhotosPage?
    public var stat: String?
}

public class FlickrApi: GalleryApi {
    
    @Published var fetchedPhotos: [Photo] = []
    @Published var apiIsUnresponsiveOrHasError = false
    
    public let photosPerPage = 50
    public var lastFetchedPageIndex = 0
    private var totalPages = 0
    
    
    private let apiUrl = "https://api.flickr.com/services/rest/"
    private let apiKey = "25b67dd5c34d3dbb44e1212493d9d910"
    
    private var isLoading = false
    
    public func getPhotosPage(page: Int?) {
        
        if isLoading {
            return
        }
        
        if let page = page {
            if page <= lastFetchedPageIndex || page >= totalPages {
                return
            }
        }
        
        let page = page ?? 0
        
        isLoading = true
        
        var parameters = ["api_key": apiKey, "format" : "json", "nojsoncallback" : "1", "method" : "flickr.photos.search", "tags": "surreal", "per_page" : "\(photosPerPage)", "sort" : "date-posted-desc"]
        
        if page > 0 {
            parameters["page"] = String(page)
        }
        
        print("GETTING PHOTOS, PAGE \(lastFetchedPageIndex)")
        
        AF.request(apiUrl, method: .get, parameters: parameters).responseJSON { response in
            
            if let error = response.error {
                print("FAILED TO GET PHOTOS PAGE")
                print(error.errorDescription ?? "")
                self.apiIsUnresponsiveOrHasError = true
                self.lastFetchedPageIndex -= 1
                return
            }
            
            guard let data = response.data else {
                self.apiIsUnresponsiveOrHasError = true
                self.lastFetchedPageIndex -= 1
                return
            }
            
            self.lastFetchedPageIndex = page
            
            self.apiIsUnresponsiveOrHasError = false
            
            let photoPage: PhotosRequestResult = try! JSONDecoder().decode(PhotosRequestResult.self, from: data)
            self.fetchedPhotos.append(contentsOf: photoPage.photos?.photo ?? [])
            
            self.totalPages = photoPage.photos?.total ?? 0
            
            for i in 0..<self.fetchedPhotos.count {
                self.fetchedPhotos[i].page = page
                self.fetchedPhotos[i].indexInPage = i
            }
            
            print("Found " + String(photoPage.photos?.total ?? 0) + " photos!")
            self.isLoading = false
        }
        
    }
    
}


// FLICKR PHOTO SEARCH JSON RESPONSE EXAMPLE:
//{ "photos": { "page": 1, "pages": "813", "perpage": 100, "total": "81283",
//    "photo": [
//      { "id": "51433110413", "owner": "39366602@N05", "secret": "8a180b7a3a", "server": "65535", "farm": 66, "title": "Sing Me a Song", "ispublic": 1,        "isfriend": 0, "isfamily": 0 },
//{ "id": "51427598530", "owner": "52357722@N00", "secret": "ddd1ce4e0c", "server": "65535", "farm": 66, "title": "Foggy day at Ogunquit Beach", "ispublic": 1, "isfriend": 0, "isfamily": 0 }
//] }, "stat": "ok" }




// FLICKR INDIVIDUAL PHOTO INFO JSON RESPONE EXAMPLE:
//{ "photo": { "id": "51431091772", "secret": "bd9e0c66a6", "server": "65535", "farm": 66, "dateuploaded": "1630984143", "isfavorite": 0, "license": 0, "safety_level": 0, "rotation": 0,
//    "owner": { "nsid": "56796376@N00", "username": "mckaysavage", "realname": "McKay Savage", "location": "Ubud, Bali, Indonesia", "iconserver": "2846", "iconfarm": 3, "path_alias": "mckaysavage" },
//    "title": { "_content": "India 2017 - Trek Markha Valley & Stok Kangri 111 Namling La" },
//    "description": { "_content": "Panorama looking out from Namling La (Stok La) back down towards Rumbak. The geology &amp; colours of the mountain terrain here is just surreal." },
//    "visibility": { "ispublic": 1, "isfriend": 0, "isfamily": 0 },
//    "dates": { "posted": "1630984143", "taken": "2017-07-19 11:46:33", "takengranularity": 0, "takenunknown": 0, "lastupdate": "1630984169" }, "views": 2,
//    "editability": { "cancomment": 0, "canaddmeta": 0 },
//    "publiceditability": { "cancomment": 1, "canaddmeta": 0 },
//    "usage": { "candownload": 0, "canblog": 0, "canprint": 0, "canshare": 1 },
//    "comments": { "_content": 0 },
//    "notes": {
//      "note": [
//
//      ] },
//    "people": { "haspeople": 0 },
//    "tags": {
//      "tag": [
//        { "id": "1122603-51431091772-1700", "author": "56796376@N00", "authorname": "mckaysavage", "raw": "geotagged", "_content": "geotagged", "machine_tag": 0 },
//        { "id": "1122603-51431091772-393", "author": "56796376@N00", "authorname": "mckaysavage", "raw": "India", "_content": "india", "machine_tag": 0 },
//        { "id": "1122603-51431091772-176591", "author": "56796376@N00", "authorname": "mckaysavage", "raw": "iPhone", "_content": "iphone", "machine_tag": 0 },
//        { "id": "1122603-51431091772-523253133", "author": "56796376@N00", "authorname": "mckaysavage", "raw": "Keyword schema subject", "_content": "keywordschemasubject", "machine_tag": 0 },
//        { "id": "1122603-51431091772-84671", "author": "56796376@N00", "authorname": "mckaysavage", "raw": "Ladakh", "_content": "ladakh", "machine_tag": 0 },
//        { "id": "1122603-51431091772-306", "author": "56796376@N00", "authorname": "mckaysavage", "raw": "Panorama", "_content": "panorama", "machine_tag": 0 },
//        { "id": "1122603-51431091772-7443", "author": "56796376@N00", "authorname": "mckaysavage", "raw": "Pass", "_content": "pass", "machine_tag": 0 }
//      ] },
//    "location": { "latitude": 34.034713, "longitude": 77.474175, "accuracy": 16, "context": 0,
//      "locality": { "_content": "Rumbak" },
//      "county": { "_content": "Leh Ladakh" },
//      "region": { "_content": "Kashmir" },
//      "country": { "_content": "India" },
//      "neighbourhood": { "_content": "" } },
//    "geoperms": { "ispublic": 1, "iscontact": 0, "isfriend": 0, "isfamily": 0 },
//    "urls": {
//      "url": [
//        { "type": "photopage", "_content": "https:\/\/www.flickr.com\/photos\/mckaysavage\/51431091772\/" }
//      ] }, "media": "photo" }, "stat": "ok" }
