//
//  Constants.swift
//  FlickrGallery
//
//  Created by The Admin on 2021-09-07.
//

import Foundation

let fallBackPhotoUrl = "https://i.insider.com/61135525ad63f30019501966?width=700&format=jpeg&auto=webp"

let navigationTitleText = "Surreal is the new Real"

let errorText = "We're having some problems. Hang on!"
