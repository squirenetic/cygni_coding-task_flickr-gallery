//
//  Location.swift
//  FlickrGallery
//
//  Created by The Admin on 2021-09-07.
//

import Foundation

// "location": { "latitude": 34.034713, "longitude": 77.474175, "accuracy": 16, "context": 0,

public struct Location: Decodable, Hashable {
    
    public var latitude: Double? = nil
    public var longitude: Double? = nil
    
}
