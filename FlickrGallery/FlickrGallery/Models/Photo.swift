//
//  Photo.swift
//  FlickrGallery
//
//  Created by The Admin on 2021-09-07.
//


import Foundation

public struct Photo: Decodable, Identifiable {
    
    public var id: UUID? = UUID()
    public var photoId: String? = nil
    public var title: String? = nil
    public var username: String? = nil
    
    public var location: Location? = nil
    
    public var page: Int? = 0
    public var indexInPage: Int? = 0
    
    private var secret: String? = nil
    private var server: String? = nil
    
    public var thumbnailUrl: String? {
        get {
            if let photoId = photoId, let secret = secret, let server = server {
                return "https://live.staticflickr.com/\(server)/\(photoId)_\(secret)_q.jpg"
            } else {
                return fallBackPhotoUrl
            }
        }
    }
    
    public var largeSizeUrl: String? {
        get {
            if let photoId = photoId, let secret = secret, let server = server {
                return "https://live.staticflickr.com/\(server)/\(photoId)_\(secret)_b.jpg"
            } else {
                return fallBackPhotoUrl
            }
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case photoId = "id" // necessary to not get problems with duplicate id:s
        case title = "title"
        case username = "username"
        case location = "location"
        case secret = "secret"
        case server = "server"
        
    }
}



