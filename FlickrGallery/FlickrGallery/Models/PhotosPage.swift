//
//  PhotoList.swift
//  FlickrGallery
//
//  Created by The Admin on 2021-09-07.
//

import Foundation

public struct PhotosPage: Decodable {    
    
    public var photo: [Photo] = []
    public var page: Int = 0
    public var pages: Int = 0
    public var total: Int = 0
    public var perpage: Int = 0
    
    
}

// FLICKR PHOTO SEARCH JSON RESPONSE EXAMPLE:
//{ "photos": { "page": 1, "pages": "813", "perpage": 100, "total": "81283",
//    "photo": [
//      { "id": "51433110413", "owner": "39366602@N05", "secret": "8a180b7a3a", "server": "65535", "farm": 66, "title": "Sing Me a Song", "ispublic": 1,        "isfriend": 0, "isfamily": 0 },
//{ "id": "51427598530", "owner": "52357722@N00", "secret": "ddd1ce4e0c", "server": "65535", "farm": 66, "title": "Foggy day at Ogunquit Beach", "ispublic": 1, "isfriend": 0, "isfamily": 0 }
//] }, "stat": "ok" }

