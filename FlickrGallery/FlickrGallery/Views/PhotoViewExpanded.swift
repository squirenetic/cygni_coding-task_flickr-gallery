//
//  PhotoViewExpanded.swift
//  FlickrGallery
//
//  Created by The Admin on 2021-09-07.
//

import SwiftUI

struct PhotoViewExpanded: View {
    var urlString: String = ""
    var isErrorScreen: Bool = false
    
    @Binding var isPresented: Bool
    
    var url: URL {
        get {
            URL(string: urlString) ?? URL(string: fallBackPhotoUrl)!
        }
    }
    
    
    var body: some View {
        Button(action: {
            
            isPresented = false
        }) {
            
            ZStack {
                Rectangle()
                    .fill(Color.black)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
                    .edgesIgnoringSafeArea(.all)
                AsyncImage<ProgressView>(
                    url: url,
                    placeholder:
                        ProgressView()
                ).aspectRatio(contentMode: .fit).foregroundColor(.white)
                Text(isErrorScreen ? errorText : "").foregroundColor(.red).fontWeight(.black)
                
            }
        }
    }
}

struct PhotoViewExpanded_Previews: PreviewProvider {
    
    static var previews: some View {
        PhotoViewExpanded(urlString:  fallBackPhotoUrl, isPresented: .constant(true))
    }
}
