//
//  ThumbnailView.swift
//  FlickrGallery
//
//  Created by The Admin on 2021-09-07.
//

import SwiftUI

struct ThumbnailView: View {
    
    var urlString: String = ""
    
    init(urlString: String) {
        self.urlString = urlString
    }
    
    var url: URL {
        get {
            URL(string: urlString) ?? URL(string: fallBackPhotoUrl)!
        }
    }
    
    
    var body: some View {
        AsyncImage<ProgressView>(
            url: url,
            placeholder:
                ProgressView()
        ).aspectRatio(contentMode: .fit)
    }
}

struct ThumbnailView_Previews: PreviewProvider {
    static var previews: some View {
        ThumbnailView(urlString:  fallBackPhotoUrl)
    }
}
