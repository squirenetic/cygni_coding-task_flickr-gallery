//
//  ContentView.swift
//  FlickrGallery
//
//  Created by The Admin on 2021-09-07.
//

import SwiftUI

struct ContentView: View {
    
    var body: some View {
        
        GalleryView()
    }
    
}

struct ContentView_Previews: PreviewProvider {
    
    static var previews: some View {
        ContentView()
            .environmentObject(FlickrApi())
    }
}
