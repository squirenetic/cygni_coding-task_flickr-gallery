//
//  AsyncImage.swift
//  FlickrGallery
//
//  Created by The Admin on 2021-09-07.
//

import SwiftUI

struct AsyncImage<Placeholder: View>: View {
    
    @StateObject private var loader: ImageLoader
    
    private let placeholder: Placeholder
    
    init(url: URL, placeholder: Placeholder) {
        self.placeholder = placeholder
        _loader = StateObject(wrappedValue: ImageLoader(url: url))
    }
    
    var body: some View {
        content
            .onAppear(perform: loader.load)
    }
    
    private var content: some View {
        Group {
            if loader.image != nil {
                Image(uiImage: loader.image!)
                    .resizable()
            } else {
                placeholder.frame(width: 100, height: 100, alignment: .center)
            }
        }
    }
}

