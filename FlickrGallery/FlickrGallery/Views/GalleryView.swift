//
//  GalleryView.swift
//  FlickrGallery
//
//  Created by The Admin on 2021-09-07.
//

import SwiftUI

struct GalleryView: View {
    
    @EnvironmentObject var api: FlickrApi
    
    @State var showingEnlargedView: Bool = false
    @State var currentlyViewedPhoto: Photo?
    
    private var gridItemLayout =
        [
            GridItem(.flexible(minimum: 100, maximum: 500)),
            GridItem(.flexible(minimum: 100, maximum: 500)),
            GridItem(.flexible(minimum: 100, maximum: 500))
        ]
    
    var body: some View {
        ZStack {
            NavigationView {
                
                ScrollView {
                    
                    LazyVGrid(columns: gridItemLayout, alignment: .center, spacing: nil, pinnedViews: [], content: {
                        ForEach(api.fetchedPhotos) { photo in
                            Button(action: {
                                currentlyViewedPhoto = photo
                                showingEnlargedView = true
                            }) {
                                
                                ThumbnailView(urlString: photo.thumbnailUrl!)
                            }.onAppear {
                                if (photo.photoId == api.fetchedPhotos[api.fetchedPhotos.count - 20].photoId) {
                                    api.getPhotosPage(page: api.lastFetchedPageIndex + 1)
                                }
                            }
                        }
                        
                    })
                } .onAppear {
                    api.getPhotosPage(page: nil)
                }.navigationTitle("Surreal is the new Real")
            }
            
            if showingEnlargedView {
                PhotoViewExpanded(urlString: currentlyViewedPhoto?.largeSizeUrl ?? fallBackPhotoUrl, isPresented: $showingEnlargedView).animation(.easeIn.speed(0.75))
            }
            
            if api.apiIsUnresponsiveOrHasError {
                
                PhotoViewExpanded(urlString: fallBackPhotoUrl, isErrorScreen: true, isPresented: $showingEnlargedView).onAppear {
                    api.getPhotosPage(page: api.lastFetchedPageIndex + 1)
                }
            }
        }
    }
}

struct GalleryView_Previews: PreviewProvider {
    static var previews: some View {
        GalleryView()
    }
}
