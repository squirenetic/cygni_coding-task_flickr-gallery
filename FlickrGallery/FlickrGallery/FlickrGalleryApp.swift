//
//  FlickrGalleryApp.swift
//  FlickrGallery
//
//  Created by The Admin on 2021-09-07.
//

import SwiftUI

@main
struct FlickrGalleryApp: App {
    
    var api: FlickrApi = FlickrApi()
    
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(api)
        }
    }
}
